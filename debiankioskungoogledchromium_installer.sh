#!/bin/bash

# download package and install ungoogled-chromium like described here https://wiki.debian.org/ungoogled-chromium
wget https://github.com/berkley4/ungoogled-chromium-debian/releases/download/129.0.6668.100-stable1.2/ungoogled-chromium_129.0.6668.100-stable1_amd64.deb
chmod +x ungoogled-chromium_129.0.6668.100-stable1_amd64.deb
apt install ./ungoogled-chromium_129.0.6668.100-stable1_amd64.deb -y

# add repository and install ungoogled-chromium
# https://github.com/ungoogled-software/ungoogled-chromium-debian#getting-obs-packages
# echo 'deb http://download.opensuse.org/repositories/home:/ungoogled_chromium/Debian_Bullseye/ /' | sudo tee /etc/apt/sources.list.d/home-ungoogled_chromium.list > /dev/null
# curl -s 'https://download.opensuse.org/repositories/home:/ungoogled_chromium/Debian_Bullseye/Release.key' | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home-ungoogled_chromium.gpg > /dev/null
# apt update
# apt install ungoogled-chromium -y

# get software
apt-get install \
    unclutter \
    xorg \
    openbox \
    lightdm \
    locales \
    xdotool \
    -y
    
# dir
mkdir -p /home/kiosk/.config/openbox

# create group
groupadd kiosk

# create user if not exists
id -u kiosk &>/dev/null || useradd -m kiosk -g kiosk -s /bin/bash 

# rights
chown -R kiosk:kiosk /home/kiosk

# remove virtual consoles
if [ -e "/etc/X11/xorg.conf" ]; then
  mv /etc/X11/xorg.conf /etc/X11/xorg.conf.backup
fi
cat > /etc/X11/xorg.conf << EOF
Section "ServerFlags"
    Option "DontVTSwitch" "true"
EndSection
EOF

# create config
if [ -e "/etc/lightdm/lightdm.conf" ]; then
  mv /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.backup
fi
cat > /etc/lightdm/lightdm.conf << EOF
[SeatDefaults]
autologin-user=kiosk
EOF

# create autostart
if [ -e "/home/kiosk/.config/openbox/autostart" ]; then
  mv /home/kiosk/.config/openbox/autostart /home/kiosk/.config/openbox/autostart.backup
fi
cat > /home/kiosk/.config/openbox/autostart << EOF
#!/bin/bash
# other users can connect to x
xhost +
unclutter -grab -root &
while :
do
  chromium \
    --no-first-run \
    --start-maximized \
    --window-position=0,0 \
    --window-size=1024,768 \
    --disable \
    --disable-translate \
    --disable-infobars \
    --disable-suggestions-service \
    --disable-save-password-bubble \
    --disable-session-crashed-bubble \
    --incognito \
    --kiosk "https://huraxdaxdax.de/"
  sleep 5
done &
EOF

# Reloadscript
cat > /home/kiosk/reload.sh << EOF
export DISPLAY=:"0.0" && xdotool key ctrl+F5 && xset dpms force off
now="$(date '+%d.%M.%Y %R:%S')"
echo "$now | reload and dpms done!"
EOF

# make it executable
chmod +x /home/kiosk/reload.sh

# Reload every day at 04:00
cat > /etc/cron.d/everyDayAt4 << EOF
0 4 * * * root /bin/sh /home/kiosk/reload.sh >> /var/log/everyDayAt4.log 2>&1
EOF

echo "Done!"