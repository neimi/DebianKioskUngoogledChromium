# DebianKioskUngoogledChromium

Skript zur Installation von Ungoogled Chromium im Kiosk Mode(Vollbild, Autostart usw.) auf Debian

Benutzung

    Minimales Debian ohne Display Manager installieren

    Login als root oder mit root Rechten

    Installerdatei downloaden, ausführbar machen und ausführen

    wget https://codeberg.org/neimi/DebianKioskUngoogledChromium/raw/branch/main/debiankioskungoogledchromium_installer.sh; chmod +x debiankioskungoogledchromium_installer.sh; ./debiankioskungoogledchromium_installer.sh

If you are installing to a Raspberry Pi, change chromium to chromium-browser in the install script (both in apt line and startup command)

What will it do?

It will create a normal user kiosk, install software (check the script) and setup configs (it will backup existing) so that on reboot the kiosk user will login automaticaly and run chromium in kiosk mode with one url. It will also hide the mouse.
Change the url

Change the url at the bottom of the script, where it sais https://huraxdaxdax.de/
Is it secure?

No. Although it will run as a normal user (and I suggest you don't leave a keyboard and mouse hanging around), there will be the possibility of plugin' in a mini keyboard, opening a terminal and opening some nasty things. Security is your thing ;-)

Also Vorlage diente https://github.com/josfaber/debian-kiosk-installer
