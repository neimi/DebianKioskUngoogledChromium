#!/bin/bash

# remove dir and autostart
rm -R /home/kiosk/.config/openbox

# remove config
if [ -e "/etc/lightdm/lightdm.conf" ]; then
  rm /etc/lightdm/lightdm.conf
fi
if [ -e "/etc/lightdm/lightdm.conf.backup" ]; then
  mv /etc/lightdm/lightdm.conf.backup /etc/lightdm/lightdm.conf
fi

# set virtual consoles
if [ -e "/etc/X11/xorg.conf" ]; then
  rm /etc/X11/xorg.conf
fi
if [ -e "/etc/X11/xorg.conf.backup" ]; then
  mv /etc/X11/xorg.conf.backup /etc/X11/xorg.conf
fi

# remove software
apt remove \
    unclutter \
    xorg \
    openbox \
    lightdm \
    locales \
    -y
    
# remove ungoogled-chromium
apt remove ungoogled-chromium -y

# delete repo and key
rm /etc/apt/sources.list.d/home-ungoogled_chromium.list /etc/apt/trusted.gpg.d/home-ungoogled_chromium.gpg


# delete user
deluser --remove-home kiosk

echo "Ungoogled Chromium removed!"